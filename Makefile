# Makefile for loraserver + lora-gateway-bridge + lora-app-server

########### loraserver ##########

install_file:
        useradd -M -r -s /bin/false loraserver
        mkdir -p /opt/loraserver/bin
        mv /file/loraserver /opt/loraserver/bin

# Install the systemd service, enables it and starts it
install_systemd:
        cp cmd/systemd/loraserver.service /etc/systemd/system/
        systemctl enable /etc/systemd/system/loraserver.service
        systemctl start loraserver.service

start_loraserver:
        mkdir /etc/systemd/system/loraserver.service.d

install_upstart: start_loraserver
        cp cmd/upstart/loraserver.service.d/loraserver.conf /etc/systemd/system/loraserver.service.d/
        start loraserver
########## loraserver ##########

########## lora-gateway-bridge ##########

install_file:
        useradd -M -r -s /bin/false gatewaybridge
        mkdir -p /opt/lora-gateway-bridge/bin
        mv /file/lora-gateway-bridge /opt/lora-gateway-bridge/bin

# Install the systemd service, enables it and starts it
install_systemd:
        cp cmd/systemd/lora-gateway-bridge.service /etc/systemd/system/
        systemctl enable /etc/systemd/system/lora-gateway-bridge.service
        systemctl start lora-gateway-bridge.service

start_lora-gateway-bridge:
        mkdir /etc/systemd/system/lora-gateway-bridge.service.d

install_upstart: start_lora-gateway-bridge
        cp cmd/upstart/lora-gateway-bridge.service.d/lora-gateway-bridge.conf /etc/systemd/system/lora-gateway-bridge.service.d/
        start lora-gateway-bridge
########## lora-gateway-bridge ##########

########## lora-app-server ##########

install_file:
        useradd -M -r -s /bin/false/ appserver
        mkdir -p /opt/lora-app-server/bin
        mv /file/lora-app-server /opt/lora-app-server/bin

# Install the systemd service, enables it and starts it
install_systemd:
        cp cmd/systemd/lora-app-server.service /etc/systemd/system/
        systemctl enable /etc/systemd/system/lora-app-server.service
        systemctl start lora-app-server.service

start_lora-app-server:
        mkdir /etc/systemd/system/lora-app-server.service.d

install_upstart: start_lora-app-server
        cp cmd/upstart/lora-app-server.service.d/lora-gateway-bridge.conf /etc/systemd/system/lora-app-server.service.d/
        start lora-app-server
########## lora-app-server ##########

