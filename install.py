import shutil
import os

# Installing loraserver
def run_loraserver():
	os.system("useradd -M -r -s /bin/false loraserver")

	if not os.path.exists("/opt/loraserver/bin"):
		os.makedirs("/opt/loraserver/bin")

		shutil.move("makefileproject1/file/loraserver", "/opt/loraserver/bin")

	shutil.copyfile("/cmd/systemd/loraserver.service", "/etc/systemd/system/loraserver.service")

	if not os.path.exists("/etc/systemd/system/loraserver.service.d"):
		os.makedirs("/etc/systemd/system/loraserver.service.d")

		shutil.copyfile("/cmd/upstart/loraserver.service.d/loraserver.conf", "/etc/systemd/system/loraserver.service.d/loraserver.conf")

# Installing lora-gateway-bridge
def run_loragatewaybridge():
	os.system("useradd -M -r -s /bin/false gatewaybridge")

	if not os.path.exists("/opt/lora-gateway-bridge/bin"):
		os.makedirs("/opt/lora-gateway-bridge/bin")

		shutil.move("makefileproject1/file/lora-gateway-bridge", "/opt/lora-gateway-bridge/bin")

	shutil.copyfile("/cmd/systemd/lora-gateway-bridge.service", "/etc/systemd/system/lora-gateway-bridge.service")

	if not os.path.exists("/etc/systemd/system/lora-gateway-bridge.service.d"):
		os.makedirs("/etc/systemd/system/lora-gateway-bridge.service.d")

		shutil.copyfile("/cmd/upstart/lora-gateway-bridge.service.d/lora-gateway-bridge.conf", "/etc/systemd/system/lora-gateway-bridge.service.d/lora-gateway-bridge.conf")

# Installing lora-app-server
def run_loraappserver():
	os.system("useradd -M -r -s /bin/false appserver")

	if not os.path.exists("/opt/lora-app-server/bin"):
		os.makedirs("/opt/lora-app-server/bin")

		shutil.move("makefileproject1/file/lora-app-server", "/opt/lora-app-server/bin")

	shutil.copyfile("/cmd/systemd/lora-app-server.service", "/etc/systemd/system/lora-app-server.service")

	if not os.path.exists("/etc/systemd/system/lora-app-server.service.d"):
		os.makedirs("/etc/systemd/system/lora-app-server.service.d")

		shutil.copyfile("/cmd/upstart/lora-app-server.service.d/lora-app-server.conf", "/etc/systemd/system/lora-app-server.service.d/lora-app-server.conf")

run_loraserver()
run_loragatewaybridge()
run_loraappserver()
		

